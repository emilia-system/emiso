# Emilia Emiso

[![pipeline status](https://gitlab.com/emilia-system/emiso/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emiso/commits/master)
[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)

## Installation

Generate the package using PKGBUILD and install it. Choose a place in your files
and copy the folder /usr/share/emilia/base-iso to any place you desire.

## Use

Execute pre-build.sh without sudo first, so the local-repository can be built. Will be changed when Emilia gets a repository.

After, run build.sh with sudo.

Use ./build.sh -h for a list of options.

WARNING - Disable baloo if kde or dolphin is installed/in use! use 

``` bash
   balooctl disable
```

to disable it

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)

# Emilia Emiso - pt_BR

## Instalação

Gere o pacote usando o PKGBUILD e instale. Escolha um lugar nos seus arquivos
e copie a pasta /usr/share/emilia/base-iso para onde quiser.

## Uso

Execute o pre-build.sh sem sudo primeiro, para que o local-repository possa ser construido. Será mudado quando Emilia conseguir um repositório.

Depois rode build.sh com sudo.

Use ./build.sh -h para uma lista de opções.

AVISO - Desabilite baloo se kde ou dolphin estiver instalado/em uso! use

``` bash
   balooctl disable
```

para desabilitar ele.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue para que possa ser discutido.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)
