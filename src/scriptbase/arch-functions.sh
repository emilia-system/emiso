#!/bin/bash


# Finally back after some months. Let's see what arch is up to!

# Notes for future fixes from official archiso:
#  mkdir -p dir --> install -d -m 0755 -o 0 -g 0 -- dir
#  cleanup deletes entire boot folder! we could do that if we preserve the vmlinuz...
#  They create a entire 32 GB ext4.fs for the img generation. Maybe we should study that.
#  Something Something ARCHISO_GNUPG_FD related in archiso script.
#  SA _make_customize_airootfs for how to deal with /etc/skel in a custom way...
#  Maybe rootfs/etc/systemd/networks on releng are usefull...?
#   OM... this systemd folder is loaded now! see if anything brokes b4 adding blindly
#  scripts/run_archiso.sh uses QEMU to run the iso...? interesting...

umount_merged(){
    # TIP: If proc refuses to umount, check for process running in them. baloo is a example.
    mountpoint "${work_dir}"/x86_64/merged/ &> /dev/null && umount -Rdf -- "${work_dir}"/x86_64/merged/
    rm -rf "${work_dir}"/x86_64/work/*
}

umount_merged_error(){
    umount_merged
    exit 2
}

packages_to_install=()
pacstrap_function(){
    # $1 is with one to run
    local command_name="${1}"
    local install_dir="merged"
    case "${command_name}" in
        rootfs)
            # Arch linux originals/recommended/obligatory/etc with our touch
            packages_to_install=(base base-devel vim nano syslinux memtest86+ mkinitcpio mkinitcpio-nfs-utils efitools systemd-resolvconf) 
            # mkinitcpio-archiso TESTING removal of this package
            # Emilia and the custom packages
            packages_to_install+=(emilia-base $(grep -h -v ^# "${script_path}"/packages/arch.core))
            install_dir="rootfs"
            ;;
        commonfs)
            packages_to_install=($(grep -h -v ^# "${script_path}"/packages/arch.common))
            ;;
        desktopfs)
            packages_to_install=(emilia-meta-${desk} emilia-meta-${desk}-apps)
            ;;
        liveonlyfs)
            packages_to_install=($(grep -h -v ^# "${script_path}"/packages/arch.live) $(grep -h -v ^# "${script_path}"/packages/arch.calamares))
            ;;
        liveonlyminimal)
            packages_to_install=(openbox xorg-server lightdm-gtk-greeter $(grep -h -v ^# "${script_path}"/packages/arch.live) $(grep -h -v ^# "${script_path}"/packages/arch.calamares))
            ;;
        *)
            show_error "$(eval_gettext "Pacstrap didn't recived the correct command")" "$(eval_gettext "Check for the pacstrap_function call, and see if it's a valid command.")"
            _usage 1
            ;;
    esac
    # Check for repeated packages. NO GROUPS ALLOWED
    remove_duplicated_packages "${command_name}"
    
    # Install them
    if [ $quiet -eq 0 ]; then
        pacstrap -C "${work_dir}/pacman.conf" -c -G -M -- "${work_dir}/x86_64/${install_dir}" ${packages_to_install[@]}
    else
        pacstrap -C "${work_dir}/pacman.conf" -c -G -M -- "${work_dir}/x86_64/${install_dir}" ${packages_to_install[@]} &> /dev/null
    fi
}

# Grabs the packages_to_install array and remove duplicates
# So pacstrap don't try to reinstall packages. fixes issue #4
remove_duplicated_packages(){
    #1 is the command_name.
    local packages_candidates=()
    case "$1" in
        rootfs)
            # root don't have a "before". Do nothing.
            return
            ;;
        commonfs | liveonlyminimal)
            #check for rootfs
            packages_candidates=($(grep -h -v ^# "${out_dir}"/package_list/base.txt))
            ;;
        desktopfs)
            #check for common
            packages_candidates=($(grep -h -v ^# "${out_dir}"/package_list/common.txt))
            ;;
        liveonlyfs)
            #check for desktop
            packages_candidates=($(grep -h -v ^# "${out_dir}"/package_list/C${desk}.txt))
            ;;
        *)
            show_error "$(eval_gettext "Pacstrap didn't recived the correct command")" "$(eval_gettext "Check for the pacstrap_function call, and see if it's a valid command.")"
            _usage 1
            ;;
    esac
        
    for _delete in "${packages_candidates[@]}"; do
        for i in "${!packages_to_install[@]}"; do
            if [ "${packages_to_install[i]}" = "$_delete" ]; then
                unset 'packages_to_install[i]'
            fi
        done
    done
}

recreate_folders(){
    # Hoping to fix the #7 issue once for all. I can't stand anymore!
    # ;& makes it execute the next.
    local folders=(work merged)
    case "$1" in
        C)
            folders+=(commonfs)
        ;&
        D)
            folders+=(desktopfs)
        ;&
        L)
            folders+=(liveonlyfs)
        ;;
        ?)
            show_error "$(eval_gettext "Wrong letter entered 'recreate_folders' function")" "$(eval_gettext "The only valid are L, D or C")"
        ;;
        *)
            show_error "$(eval_gettext "Bad use of the 'recreate_folders' function")" "$(eval_gettext "Use only one character. And they are L, D or C")"
        ;;
    esac
    
    for _dirs in "${folders[@]}"; do
        rm    -rf "${work_dir}"/x86_64/"$_dirs"
        mkdir -p  "${work_dir}"/x86_64/"$_dirs"
    done
}
#-----------------------

# Setup custom pacman.conf with current cache directories.
make_pacman_conf() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Pacman configuration")" "$(eval_gettext "Configure a new pacman.conf on the new system")"

    local _cache_dirs
    _cache_dirs="$(pacman-conf CacheDir)"
    sed -r "s|^#?\\s*CacheDir.+|CacheDir    = ${_cache_dirs[*]//$'\n'/ }|g" "/etc/pacman.conf" > "${work_dir}/pacman.conf"

}

# Base installation, plus needed packages (rootfs)
make_base_install() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Base installation")" "$(eval_gettext "Install the base group and important components")"

    mkdir -p "${work_dir}/x86_64/rootfs/etc"
    cp "${script_path}"/pacman.conf "${work_dir}"/x86_64/rootfs/etc

    # Put yay inside final iso.
    cp "${script_path}"/repository/yay* "${work_dir}"/x86_64/rootfs/usr/share/local-repository/
    
    pacstrap_function rootfs || pacstrap_function rootfs || umount_merged_error
}

make_local_repository(){
    rm -rf "${work_dir}/x86_64/rootfs/usr/share/local-repository/*" 
    mkdir -p "${work_dir}/x86_64/rootfs/usr/share/local-repository" 
    cp -r "${script_path}/repository/." "${work_dir}/x86_64/rootfs/usr/share/local-repository/"
}

make_base_config(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Base configuration")" "$(eval_gettext "Configure the pacman and parts of the admninistration")"
    
    cp -drf "${script_path}"/fs/rootfs "${work_dir}"/x86_64
    
    [ $quiet -eq 0 ] && echo " - Pacman mirrorlist..."
    curl -o "${work_dir}"/x86_64/rootfs/etc/pacman.d/mirrorlist 'https://www.archlinux.org/mirrorlist/?country=all&protocol=http&use_mirror_status=on'
    
    eval arch-chroot "${work_dir}"/x86_64/rootfs "/root/customize_rootfs.sh"
    rm -f "${work_dir}"/x86_64/rootfs/root/customize_rootfs.sh
    
    pacman -Q --sysroot "${work_dir}/x86_64/rootfs" > "${out_dir}"/package_list/base.txt
}

make_common_install() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Core installation")" "$(eval_gettext "Install core components related to the graphical interface")"
    
    recreate_folders C
    
    mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/commonfs,workdir="${work_dir}"/x86_64/work \
            install_common "${work_dir}"/x86_64/merged
    
    pacstrap_function commonfs || pacstrap_function commonfs || umount_merged_error
    umount_merged
}

make_common_config(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Common configuration")" "$(eval_gettext "Configure the graphical interface and related")"
    
    cp -drf "${script_path}"/fs/commonfs "${work_dir}"/x86_64
    
    mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/commonfs,workdir="${work_dir}"/x86_64/work \
            config_common "${work_dir}"/x86_64/merged
            
    eval arch-chroot "${work_dir}"/x86_64/merged "/root/customize_commonfs.sh"
    pacman -Q --sysroot "${work_dir}/x86_64/merged" > "${out_dir}"/package_list/common.txt

    umount_merged
    rm -f "${work_dir}"/x86_64/commonfs/root/customize_commonfs.sh
}

make_desktop_install() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Install the \${desk} Desktop Enviroment")" "$(eval_gettext "Install the base packages for the desktop enviroment as listed in Emilia meta packages")"
    
    recreate_folders D
    
    mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/commonfs:"${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/desktopfs,workdir="${work_dir}"/x86_64/work \
            install_de "${work_dir}"/x86_64/merged
    
    pacstrap_function desktopfs || pacstrap_function desktopfs || umount_merged_error
    umount_merged
}

make_desktop_config(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Configuration of \${desk} Desktop Enviroment")" "$(eval_gettext "Lightdm, user desktop, themes, colors... runs Emilia Config")"
    
    cp -drf "${script_path}"/fs/desktopfs "${work_dir}"/x86_64

    mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/commonfs:"${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/desktopfs,workdir="${work_dir}"/x86_64/work \
            config_de "${work_dir}"/x86_64/merged
            
    eval arch-chroot "${work_dir}"/x86_64/merged "/root/customize_desktopfs.sh ${desk}"
    pacman -Q --sysroot "${work_dir}/x86_64/merged" > "${out_dir}"/package_list/C"${desk}".txt
    
    umount_merged
    rm "${work_dir}"/x86_64/desktopfs/root/customize_desktopfs.sh
    
    #tell others witch DE is running. Needed for calamares and AllInOne. Maybe not for long...
    mkdir -p "${work_dir}"/x86_64/desktopfs/usr/share/emilia/
    echo "${desk}" > "${work_dir}"/x86_64/desktopfs/usr/share/emilia/desktop.txt
}

make_live_install() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Installation of Live packages")" "$(eval_gettext "Installation of the packages that only the ISO will have")"
    
    recreate_folders L
    
    if [ "${desk}" = "isoMinimal" ]; then
         mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/liveonlyfs,workdir="${work_dir}"/x86_64/work \
            min_install_live "${work_dir}"/x86_64/merged
        
        pacstrap_function liveonlyminimal || pacstrap_function liveonlyminimal || umount_merged_error
    else
        mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/desktopfs:"${work_dir}"/x86_64/commonfs:"${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/liveonlyfs,workdir="${work_dir}"/x86_64/work \
            install_live "${work_dir}"/x86_64/merged
        
        #calamares tb pq adianta uns 10 min no min no all isos
        pacstrap_function liveonlyfs || pacstrap_function liveonlyfs || umount_merged_error
    fi
    umount_merged
}

make_live_config(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Setup of the Live ISO")" "$(eval_gettext "Prepares the part of the ISO that only exist in the Live")"
    
    cp -drf "${script_path}"/fs/liveonlyfs "${work_dir}"/x86_64
    
    if [ "${desk}" = "isoMinimal" ]; then
         mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/liveonlyfs,workdir="${work_dir}"/x86_64/work \
            min_config_live "${work_dir}"/x86_64/merged            
    else
        mountpoint "${work_dir}"/x86_64/merged &> /dev/null || mount -t overlay -o \
            lowerdir="${work_dir}"/x86_64/desktopfs:"${work_dir}"/x86_64/commonfs:"${work_dir}"/x86_64/rootfs,upperdir="${work_dir}"/x86_64/liveonlyfs,workdir="${work_dir}"/x86_64/work \
            config_live "${work_dir}"/x86_64/merged        
    fi
    
    # TODO - config file so you choose where to copy the local repository. Remember that ckbcomp HAS to be in the live iso.
    # Copy local repository to live system, as it's the usefull place for the current script
    cp "${script_path}"/repository/* "${work_dir}"/x86_64/merged/usr/share/local-repository/
    
    eval arch-chroot "${work_dir}"/x86_64/merged "/root/customize_livefs.sh"
    if [ "${desk}" = "isoMinimal" ]; then
        eval arch-chroot "${work_dir}"/x86_64/merged "/root/customize_livefs_minimal.sh"
    fi    
    pacman -Q --sysroot "${work_dir}/x86_64/merged" > "${out_dir}"/package_list/L"${desk}".txt

    umount_merged
    rm -f "${work_dir}"/x86_64/liveonlyfs/root/customize_livefs*.sh
}

# Copy mkinitcpio archiso hooks and build initramfs (rootfs)
make_setup_mkinitcpio() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Setup Initramfs")" "$(eval_gettext "Prepare the files for the initramfs using hooks and mkinitcpio")"

    local _hook
    mkdir -p "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks
    mkdir -p "${work_dir}"/x86_64/rootfs/etc/initcpio/install
    
    for _hook in iso_shutdown iso_pxe_common iso_pxe_nbd iso_pxe_http iso_pxe_nfs iso_loop_mnt; do    
        cp /usr/lib/initcpio/hooks/arch${_hook} "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/em${_hook}
        cp /usr/lib/initcpio/install/arch${_hook} "${work_dir}"/x86_64/rootfs/etc/initcpio/install/em${_hook}
    done
    cp /usr/lib/initcpio/install/archiso "${work_dir}"/x86_64/rootfs/etc/initcpio/install/emiso    
    
    cp /usr/lib/initcpio/install/archiso_kms "${work_dir}"/x86_64/rootfs/etc/initcpio/install/emiso_kms
    cp /usr/lib/initcpio/archiso_shutdown "${work_dir}"/x86_64/rootfs/etc/initcpio/emiso_shutdown
    
    sed -i "s|/usr/lib/initcpio/|/etc/initcpio/|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/install/emiso_shutdown
    
    #------------------------troca as palavras de archiso para emilia e mesmo para airootfs.
    for _hooks in iso_shutdown iso_pxe_common iso_pxe_nbd iso_pxe_http iso_pxe_nfs iso_loop_mnt; do
        for _path in install hooks; do
             sed -i "s|archiso|emiso|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/${_path}/em${_hooks}
             sed -i "s|airootfs|rootfs|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/${_path}/em${_hooks}
        done
    done
    
    for _dirs in install/emiso_kms emiso_shutdown install/emiso; do
        sed -i "s|archiso|emiso|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/${_dirs}
        sed -i "s|airootfs|rootfs|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/${_dirs}
    done
    
    #---------------------------------------
    
    cp "${script_path}"/mkinitcpio.conf "${work_dir}"/x86_64/rootfs/etc/mkinitcpio-emiso.conf
}

# Generate initramfs
make_gen_initram(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Generation of the Initramfs")" "$(eval_gettext "Generate the initram itself")"
    
    rm -f "${work_dir}"/iso/"${install_dir}"/boot/x86_64/emiso.img
    rm -f "${work_dir}"/x86_64/rootfs/boot/emiso.img
    rm -f "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/emiso
    
    # TESTING unified initram file
    #if [ "${desk}" = "isoMinimal" ]; then
    #    cp "${script_path}"/emiso-minimal "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/emiso
    #else
        cp "${script_path}"/emiso "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/
    #fi
    
    if [ "${script_state}" = "AllInOne" ]; then
        sed -i "s|desktopfs.sfs|desktop${desk}fs.sfs|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/emiso
        sed -i "s|liveonlyfs.sfs|liveonly${desk}fs.sfs|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/emiso    
    fi

    sed -i "s|\(cow_spacesize=\)\"256M\"|\1\"${cowspace}\"|g" "${work_dir}"/x86_64/rootfs/etc/initcpio/hooks/emiso
    
    gnupg_fd=
    if [[ ${gpg_key} ]]; then
      gpg --export "${gpg_key}" >"${work_dir}"/gpgkey
      exec 17<>"${work_dir}"/gpgkey
    fi
    ARCHISO_GNUPG_FD=${gpg_key:+17} eval arch-chroot "${work_dir}"/x86_64/rootfs "mkinitcpio -c /etc/mkinitcpio-emiso.conf -k /boot/vmlinuz-linux -g /boot/emiso.img"
    
    if [[ ${gpg_key} ]]; then
      exec 17<&-
    fi
    
    mkdir -p "${work_dir}"/iso/"${install_dir}"/boot/x86_64
    
    if [ "${script_state}" = "AllInOne" ]; then
        cp "${work_dir}"/x86_64/rootfs/boot/emiso.img "${work_dir}"/iso/"${install_dir}"/boot/x86_64/emiso"${desk}".img
    else
        cp "${work_dir}"/x86_64/rootfs/boot/emiso.img "${work_dir}"/iso/"${install_dir}"/boot/x86_64/emiso.img
    fi
}

# Prepare kernel ${install_dir}/boot/. Initram is in make_gen_initram
make_boot() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Boot Setup")" "$(eval_gettext "Prepare kernel and initramfs for booting the ISO")"
    
    mkdir -p "${work_dir}"/iso/"${install_dir}"/boot/x86_64
    
    cp "${work_dir}"/x86_64/rootfs/boot/vmlinuz-linux "${work_dir}"/iso/"${install_dir}"/boot/x86_64/vmlinuz

    # ---------------------------------------------
    # INTEL AND AMD CODE + LICENSING 
    cp "${work_dir}"/x86_64/rootfs/boot/memtest86+/memtest.bin "${work_dir}"/iso/"${install_dir}"/boot/memtest
    cp "${work_dir}"/x86_64/rootfs/usr/share/licenses/common/GPL2/license.txt "${work_dir}"/iso/"${install_dir}"/boot/memtest.COPYING
    cp "${work_dir}"/x86_64/rootfs/boot/intel-ucode.img "${work_dir}"/iso/"${install_dir}"/boot/intel_ucode.img
    cp "${work_dir}"/x86_64/rootfs/usr/share/licenses/intel-ucode/LICENSE "${work_dir}"/iso/"${install_dir}"/boot/intel_ucode.LICENSE
    cp "${work_dir}"/x86_64/rootfs/boot/amd-ucode.img "${work_dir}"/iso/"${install_dir}"/boot/amd_ucode.img
}
# Prepare /${install_dir}/boot/syslinux
make_syslinux() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Syslinux Setup")" "$(eval_gettext "Prepare syslinux for action")"

    _uname_r=$(file -b "${work_dir}"/x86_64/rootfs/boot/vmlinuz-linux| awk 'f{print;f=0} /version/{f=1}' RS=' ')
    mkdir -p "${work_dir}"/iso/"${install_dir}"/boot/syslinux
    
    for _cfg in "${script_path}"/syslinux/*.cfg; do
        sed "s|%ARCHISO_LABEL%|${iso_label}|g;
             s|%INSTALL_DIR%|${install_dir}|g" "${_cfg}" > "${work_dir}"/iso/"${install_dir}"/boot/syslinux/"${_cfg##*/}"
    done
    #remove all.
    mv "${work_dir}"/iso/"${install_dir}"/boot/syslinux/emiso_sys_all.cfg "${work_dir}"/
    
    cp "${script_path}"/syslinux/splash.png "${work_dir}"/iso/"${install_dir}"/boot/syslinux
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/*.c32 "${work_dir}"/iso/"${install_dir}"/boot/syslinux
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/lpxelinux.0 "${work_dir}"/iso/"${install_dir}"/boot/syslinux
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/memdisk "${work_dir}"/iso/"${install_dir}"/boot/syslinux
    mkdir -p "${work_dir}"/iso/"${install_dir}"/boot/syslinux/hdt
    gzip -c -9 "${work_dir}"/x86_64/rootfs/usr/share/hwdata/pci.ids > "${work_dir}"/iso/"${install_dir}"/boot/syslinux/hdt/pciids.gz
    gzip -c -9 "${work_dir}"/x86_64/rootfs/usr/lib/modules/"${_uname_r}"/modules.alias > "${work_dir}"/iso/"${install_dir}"/boot/syslinux/hdt/modalias.gz
}

# Prepare /isolinux
make_isolinux() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Isolinux Setup")" "$(eval_gettext "Copys the necessary configuration for the ISO")"

    mkdir -p "${work_dir}"/iso/isolinux
    sed "s|%INSTALL_DIR%|${install_dir}|g" "${script_path}"/isolinux/isolinux.cfg > "${work_dir}"/iso/isolinux/isolinux.cfg
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/isolinux.bin "${work_dir}"/iso/isolinux/
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/isohdpfx.bin "${work_dir}"/iso/isolinux/
    cp "${work_dir}"/x86_64/rootfs/usr/lib/syslinux/bios/ldlinux.c32 "${work_dir}"/iso/isolinux/
}
# Prepare /EFI
make_efi() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "EFI Setup")" "$(eval_gettext "Prepare the ISO for booting on EFI mode")"

    mkdir -p "${work_dir}"/iso/EFI/boot
    cp "${work_dir}"/x86_64/rootfs/usr/share/efitools/efi/PreLoader.efi "${work_dir}"/iso/EFI/boot/bootx64.efi
    cp "${work_dir}"/x86_64/rootfs/usr/share/efitools/efi/HashTool.efi "${work_dir}"/iso/EFI/boot/

    cp "${work_dir}"/x86_64/rootfs/usr/lib/systemd/boot/efi/systemd-bootx64.efi "${work_dir}"/iso/EFI/boot/loader.efi

    mkdir -p "${work_dir}"/iso/loader/entries
    cp "${script_path}"/efiboot/loader/loader.conf "${work_dir}"/iso/loader/
    cp "${script_path}"/efiboot/loader/entries/uefi-shell-v2-x86_64.conf "${work_dir}"/iso/loader/entries/
    cp "${script_path}"/efiboot/loader/entries/uefi-shell-v1-x86_64.conf "${work_dir}"/iso/loader/entries/

    sed "s|%ARCHISO_LABEL%|${iso_label}|g;
         s|%INSTALL_DIR%|${install_dir}|g" \
        "${script_path}"/efiboot/loader/entries/emiso-x86_64-usb.conf > "${work_dir}"/iso/loader/entries/emiso-x86_64.conf

    # EFI Shell 2.0 for UEFI 2.3+
    echo " - Downloading Efi Shell v2..."
    curl -o "${work_dir}"/iso/EFI/shellx64_v2.efi https://raw.githubusercontent.com/tianocore/edk2/UDK2018/ShellBinPkg/UefiShell/X64/Shell.efi
    
    echo " - Downloading Efi Shell v1..."
    # EFI Shell 1.0 for non UEFI 2.3+
    curl -o "${work_dir}"/iso/EFI/shellx64_v1.efi https://raw.githubusercontent.com/tianocore/edk2/UDK2018/EdkShellBinPkg/FullShell/X64/Shell_Full.efi
}
# Prepare efiboot.img::/EFI for "El Torito" EFI boot mode
make_efiboot() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Setup EFI El Torito")" "$(eval_gettext "Boot EFI FAT mode")"
    
    mkdir -p "${work_dir}"/iso/EFI/emiso
    [ $quiet -eq 0 ] && echo "$(eval_gettext "Criating mkfs.fat for efiboot")"
    truncate -s 64M "${work_dir}"/iso/EFI/emiso/efiboot.img
    mkfs.fat -n ARCHISO_EFI "${work_dir}"/iso/EFI/emiso/efiboot.img

    [ $quiet -eq 0 ] && echo "$(eval_gettext "mkfs.fat ready")"
    
    mkdir -p "${work_dir}"/efiboot
    mount "${work_dir}"/iso/EFI/emiso/efiboot.img "${work_dir}"/efiboot

    mkdir -p "${work_dir}"/efiboot/EFI/emiso
    cp "${work_dir}"/iso/"${install_dir}"/boot/x86_64/vmlinuz   "${work_dir}"/efiboot/EFI/emiso/vmlinuz.efi
    cp "${work_dir}"/iso/"${install_dir}"/boot/x86_64/emiso.img "${work_dir}"/efiboot/EFI/emiso/emiso.img

    cp "${work_dir}"/iso/"${install_dir}"/boot/intel_ucode.img "${work_dir}"/efiboot/EFI/emiso/intel_ucode.img
    cp "${work_dir}"/iso/"${install_dir}"/boot/amd_ucode.img   "${work_dir}"/efiboot/EFI/emiso/amd_ucode.img
    
    mkdir -p "${work_dir}"/efiboot/EFI/boot
    cp "${work_dir}"/x86_64/rootfs/usr/share/efitools/efi/PreLoader.efi "${work_dir}"/efiboot/EFI/boot/bootx64.efi
    cp "${work_dir}"/x86_64/rootfs/usr/share/efitools/efi/HashTool.efi  "${work_dir}"/efiboot/EFI/boot/

    cp "${work_dir}"/x86_64/rootfs/usr/lib/systemd/boot/efi/systemd-bootx64.efi "${work_dir}"/efiboot/EFI/boot/loader.efi

    mkdir -p "${work_dir}"/efiboot/loader/entries
    cp "${script_path}"/efiboot/loader/loader.conf "${work_dir}"/efiboot/loader/
    cp "${script_path}"/efiboot/loader/entries/uefi-shell-v2-x86_64.conf "${work_dir}"/efiboot/loader/entries/
    cp "${script_path}"/efiboot/loader/entries/uefi-shell-v1-x86_64.conf "${work_dir}"/efiboot/loader/entries/

    sed "s|%ARCHISO_LABEL%|${iso_label}|g;
         s|%INSTALL_DIR%|${install_dir}|g" \
        "${script_path}"/efiboot/loader/entries/emiso-x86_64-cd.conf > "${work_dir}"/efiboot/loader/entries/emiso-x86_64.conf

    cp "${work_dir}"/iso/EFI/shellx64_v2.efi "${work_dir}"/efiboot/EFI/
    cp "${work_dir}"/iso/EFI/shellx64_v1.efi "${work_dir}"/efiboot/EFI/

    umount -rdf -- "${work_dir}"/efiboot
}

# Cleanup useless files
make_cleanup(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Root Cleaning")" "$(eval_gettext "Cleaning of useless files to reduce ISO size")"

    for _fs in rootfs commonfs desktopfs liveonlyfs; do
        # Delete initcpio image(s)
        if [ -d  "${work_dir}/x86_64/${_fs}/boot" ]; then
            find "${work_dir}/x86_64/${_fs}/boot" -type f -name '*.img' -delete
        fi
        # Delete pacman database sync cache files (*.tar.gz)
        if [ -d  "${work_dir}/x86_64/${_fs}/var/lib/pacman" ]; then
            find "${work_dir}/x86_64/${_fs}/var/lib/pacman" -maxdepth 1 -type f -delete
        fi
        # Delete pacman database sync cache
        if [ -d  "${work_dir}/x86_64/${_fs}/var/lib/pacman/sync" ]; then
            find "${work_dir}/x86_64/${_fs}/var/lib/pacman/sync" -delete
        fi
        # Delete pacman package cache
        if [ -d  "${work_dir}/x86_64/${_fs}/var/cache/pacman/pkg" ]; then
            find "${work_dir}/x86_64/${_fs}/var/cache/pacman/pkg" -type f -delete
        fi
        # Delete all log files, keeps empty dirs.
        if [ -d  "${work_dir}/x86_64/${_fs}/var/log" ]; then
            find "${work_dir}/x86_64/${_fs}/var/log" -type f -delete
        fi
        # Delete all temporary files and dirs
        if [ -d  "${work_dir}/x86_64/${_fs}/var/tmp" ]; then
            find "${work_dir}/x86_64/${_fs}/var/tmp" -mindepth 1 -delete
        fi
        
        # TESTING FOR DELETION
        # Delete machine id. probally systemd 240 error
        #if [ -f   "${work_dir}/x86_64/${_fs}/var/lib/dbus/machine-id" ]; then
        #    rm -f "${work_dir}/x86_64/${_fs}/var/lib/dbus/machine-id"
        #fi
        
        # FIXED...? Creating empty value on the /etc version of machine id
        printf '' > "${work_dir}/x86_64/rootfs/etc/machine-id"
        
    done
    
    # Delete package pacman related files.
    find "${work_dir}" \( -name "*.pacnew" -o -name "*.pacsave" -o -name "*.pacorig" \) -delete
}

make_prepare_base(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Prepare Base")" "$(eval_gettext "Build SquashFS for the new root")"
    
    # Make sure everyone here is root owned.
    chown -- 0:0 "${work_dir}/x86_64/rootfs/"
    
    mkdir -p "${work_dir}/iso/${install_dir}/x86_64"
    
    if ! [ -f "${work_dir}/iso/${install_dir}/x86_64/rootfs.sfs" ]; then
        if [ $quiet -eq 0 ]; then
            mksquashfs "${work_dir}/x86_64/rootfs" "${work_dir}/iso/${install_dir}/x86_64/rootfs.sfs" -noappend -comp "zstd"
        elif [ $quiet -lt 2 ]; then
            mksquashfs "${work_dir}/x86_64/rootfs" "${work_dir}/iso/${install_dir}/x86_64/rootfs.sfs" -noappend -comp "zstd" -no-progress
        else
            mksquashfs "${work_dir}/x86_64/rootfs" "${work_dir}/iso/${install_dir}/x86_64/rootfs.sfs" -noappend -comp "zstd" -no-progress &> /dev/null
        fi
        
        cd -- "${work_dir}/iso/${install_dir}/x86_64"
        sha512sum rootfs.sfs > rootfs.sha512
        
        if [[ ${gpg_key} ]]; then
            gpg --detach-sign --default-key "${gpg_key}" rootfs.sfs 
        fi
        cd -- "${OLDPWD}"
    fi
}

make_prepare_common(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Prepare Common filesystem")" "$(eval_gettext "Build SquashFS for the common part")"
    
    mkdir -p "${work_dir}/iso/${install_dir}/x86_64"
    if ! [ -f "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs" ]; then
        if [ $quiet -eq 0 ]; then
            mksquashfs "${work_dir}/x86_64/commonfs" "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs" -noappend -comp "zstd"
        elif [ $quiet -lt 2 ]; then
            mksquashfs "${work_dir}/x86_64/commonfs" "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs" -noappend -comp "zstd" -no-progress
        else
            mksquashfs "${work_dir}/x86_64/commonfs" "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs" -noappend -comp "zstd" -no-progress &> /dev/null
        fi
        
        cd -- "${work_dir}/iso/${install_dir}/x86_64"
        sha512sum commonfs.sfs > commonfs.sha512
        
        if [[ "${gpg_key}" ]]; then
            gpg --detach-sign --default-key "${gpg_key}" commonfs.sfs 
        fi
        cd -- "${OLDPWD}"
    fi
}

make_prepare_de() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Prepare the \${desk} FileSystem")" "$(eval_gettext "Build SquashFS for the new desktop")"

    if [ $quiet -eq 0 ]; then
        mksquashfs "${work_dir}/x86_64/desktopfs" "${work_dir}/iso/${install_dir}/x86_64/desktopfs.sfs" -noappend -comp "zstd"
    elif [ $quiet -lt 2 ]; then
        mksquashfs "${work_dir}/x86_64/desktopfs" "${work_dir}/iso/${install_dir}/x86_64/desktopfs.sfs" -noappend -comp "zstd" -no-progress
    else
        mksquashfs "${work_dir}/x86_64/desktopfs" "${work_dir}/iso/${install_dir}/x86_64/desktopfs.sfs" -noappend -comp "zstd" -no-progress &> /dev/null
    fi
    
    #hash checksum e gpg.
    cd -- "${work_dir}/iso/${install_dir}/x86_64"
    sha512sum desktopfs.sfs > desktopfs.sha512
    
    if [[ "${gpg_key}" ]]; then
        gpg --detach-sign --default-key "${gpg_key}" desktopfs.sfs 
    fi
    cd -- "${OLDPWD}"
}

make_prepare_live(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Prepare the Live FileSystem")" "$(eval_gettext "Build SquashFS for the new live")"
    
    
    chown -R -- 0:0 "${work_dir}/x86_64/liveonlyfs/etc"
    
    if [ $quiet -eq 0 ]; then
        mksquashfs "${work_dir}/x86_64/liveonlyfs" "${work_dir}/iso/${install_dir}/x86_64/liveonlyfs.sfs" -noappend -comp "zstd"
    elif [ $quiet -lt 2 ]; then
        mksquashfs "${work_dir}/x86_64/liveonlyfs" "${work_dir}/iso/${install_dir}/x86_64/liveonlyfs.sfs" -noappend -comp "zstd" -no-progress
    else
        mksquashfs "${work_dir}/x86_64/liveonlyfs" "${work_dir}/iso/${install_dir}/x86_64/liveonlyfs.sfs" -noappend -comp "zstd" -no-progress &> /dev/null
    fi
    
    #hash checksum e gpg.
    cd -- "${work_dir}/iso/${install_dir}/x86_64"
    sha512sum liveonlyfs.sfs > liveonlyfs.sha512
    
    if [[ "${gpg_key}" ]]; then
        gpg --detach-sign --default-key "${gpg_key}" liveonlyfs.sfs 
    fi
    cd -- "${OLDPWD}"
}

# Create an ISO9660 filesystem from "iso" directory.
make_iso() {
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "ISO creation")" "$(eval_gettext "Finally! The ISO is being created!")"
    
    local img_name="${iso_name}-${desk}.iso"
    local _iso_efi_boot_args=""

    if ! [ -f "${work_dir}/iso/isolinux/isolinux.bin" ]; then
        echo "The file '${work_dir}/iso/isolinux/isolinux.bin' does not exist." 
        exit 1
    fi
    if ! [ -f "${work_dir}/iso/isolinux/isohdpfx.bin" ]; then
        echo "The file '${work_dir}/iso/isolinux/isohdpfx.bin' does not exist." 
        exit 1
    fi

    # If exists, add an EFI "El Torito" boot image (FAT filesystem) to ISO-9660 image.
    if [ -f "${work_dir}/iso/EFI/archiso/efiboot.img" ]; then
        _iso_efi_boot_args="-eltorito-alt-boot
                            -e EFI/archiso/efiboot.img
                            -no-emul-boot
                            -isohybrid-gpt-basdat"
    fi
    
    if [ "${desk}" = "isoMinimal" ]; then
        [ -f "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs"    ] && mv "${work_dir}/iso/${install_dir}/x86_64/commonfs.sfs"     "${work_dir}/"
        [ -f "${work_dir}/iso/${install_dir}/x86_64/commonfs.sha512" ] && mv "${work_dir}/iso/${install_dir}/x86_64/commonfs.sha512"  "${work_dir}/"
        [ -f "${work_dir}/iso/${install_dir}/x86_64/desktop.sfs"     ] && mv "${work_dir}/iso/${install_dir}/x86_64/desktopfs.sfs"    "${work_dir}/"
        [ -f "${work_dir}/iso/${install_dir}/x86_64/desktop.sha512"  ] && mv "${work_dir}/iso/${install_dir}/x86_64/desktopfs.sha512" "${work_dir}/"
    fi
    
    mkdir -p "${out_dir}"
    xorriso -as mkisofs \
        -iso-level 3 \
        -full-iso9660-filenames \
        -volid "${iso_label}" \
        -appid "${iso_application}" \
        -publisher "${iso_publisher}" \
        -preparer "Prepared by mkarchiso with Emilia touches." \
        -eltorito-boot isolinux/isolinux.bin \
        -eltorito-catalog isolinux/boot.cat \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -isohybrid-mbr "${work_dir}"/iso/isolinux/isohdpfx.bin \
        ${_iso_efi_boot_args} \
        -output "${out_dir}/${img_name}" \
        "${work_dir}/iso/"
    [ $quiet -lt 2 ] || echo "$(eval_gettext "Ready!")" | ls -sh "${out_dir}/${img_name}"

    
    # Save the hashes.
    [ $quiet -eq 0 ] && echo "$(eval_gettext "Generating the hashes...")"
    sha512sum "$out_dir/$img_name" | cut -d " " -f 1  > "$out_dir/$img_name".sha512
    [ $quiet -eq 0 ] && echo "$(eval_gettext "Hash generation done!")"
    
    if [ "${desk}" = "isoMinimal" ]; then
        [ -f "${work_dir}/commonfs.sfs"     ] && mv "${work_dir}/commonfs.sfs"     "${work_dir}/iso/${install_dir}/x86_64/"
        [ -f "${work_dir}/commonfs.sha512"  ] && mv "${work_dir}/commonfs.sha512"  "${work_dir}/iso/${install_dir}/x86_64/"
        [ -f "${work_dir}/desktopfs.sfs"    ] && mv "${work_dir}/desktopfs.sfs"    "${work_dir}/iso/${install_dir}/x86_64/"
        [ -f "${work_dir}/desktopfs.sha512" ] && mv "${work_dir}/desktopfs.sha512" "${work_dir}/iso/${install_dir}/x86_64/"
        echo
    fi
}

# bonus. multiple isos
make_reset_de(){
    [ $quiet -eq 0 ] && show_step "$(eval_gettext "Prepare the Work folder")" "$(eval_gettext "Removing files from the last Desktop Enviroment (\${desk}) so a new one can enter.")"
    
    rm -rf "${work_dir}"/x86_64/desktopfs/*
    rm -rf "${work_dir}"/x86_64/liveonlyfs/*
    
    if [ ${all} -eq "1" ]; then
        mkdir -p "${work_dir}"/tempiso
        
        if [ "${desk}" != "isoMinimal" ]; then
            mv "${work_dir}"/iso/"${install_dir}"/x86_64/desktopfs.sfs    "${work_dir}"/tempiso/desktop"${desk}"fs.sfs
            mv "${work_dir}"/iso/"${install_dir}"/x86_64/desktopfs.sha512 "${work_dir}"/tempiso/desktop"${desk}"fs.sha512
        fi
        
        mv "${work_dir}"/iso/"${install_dir}"/x86_64/liveonlyfs.sfs    "${work_dir}"/tempiso/liveonly"${desk}"fs.sfs
        mv "${work_dir}"/iso/"${install_dir}"/x86_64/liveonlyfs.sha512 "${work_dir}"/tempiso/liveonly"${desk}"fs.sha512
        
        # rm -f "${work_dir}"/build.make_gen_initram
    else
        rm -f "${work_dir}"/iso/"${install_dir}"/x86_64/liveonlyfs.sfs
        rm -f "${work_dir}"/iso/"${install_dir}"/x86_64/desktopfs.sfs
    fi

    # Only remove this one if the script is in AllInOne mode. the initram generated for the desktop and minimal is unified.
    if [ "$script_state" = "AllInOne" ]; then
        rm -f "${work_dir}"/build.make_gen_initram
    fi
    
    # Remove locks
    rm -f "${work_dir}"/build.make_live_*
    rm -f "${work_dir}"/build.make_prepare_live
    rm -f "${work_dir}"/build.make_desktop_*
    rm -f "${work_dir}"/build.make_prepare_de
    
    rm -f "${work_dir}"/build.make_cleanup
    rm -f "${work_dir}"/build.make_iso
}

mkdir -p "${out_dir}"/package_list \
         "${work_dir}"/iso/"${install_dir}"/package-list
