#!/bin/bash

userSetup(){
    useradd -m emilialive
    echo "emilialive:emilialive" | chpasswd emilialive

    gpasswd -a emilialive autologin
    gpasswd -a emilialive audio
    gpasswd -a emilialive admin
    
    dconf update
}

# Update local repository to have whatever the live iso contains
cd /usr/share/local-repository
repose -zf local-repository
cd - > /dev/null

# SystemD Live configuration
sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

# Lightdm auto-login
sed -i 's/#\(autologin-user=\)/\1emilialive/' /etc/lightdm/lightdm.conf

autostart="/etc/skel/.config/autostart"
emilia-create-autostart -i -c "/usr/share/emilia/setuplive-asuser.sh"\
                           -n "liveAsUser" \
                           -d "live setup at user level (emilialive)" \
                           -a "${autostart}/"
emilia-create-autostart -i -c "sudo /usr/share/emilia/setuplive.sh" \
                           -n "liveSystem" \
                           -d "live setup at system level." \
                           -a "${autostart}/"

emilia-create-autostart -i -c "cp /usr/share/emilia/\\\"Emilia Manual pt_BR.pdf\\\" ~/" \
                           -n "Copying the manual" \
                           -d "This is the copying of the manual to your home." \
                           -a "${autostart}/"

# Create if not exists
id -u emilialive 2> /dev/null || userSetup

# Current ckbcomp method of installation is to use 
#  local-repository. So please run pre-build.sh before this
#  and pacman will pull the package. Or else, you will use yay!
#  OR ELSE, it will be installed by hand!

# Install ckbcomp via yay if local repository is not present, and if yay fails, install by hand!
 pacman -Sy ckbcomp --noconfirm --needed || su emilialive -c "yay -Sy ckbcomp --noconfirm  --needed" || {
     # Manual build. Use if Yay has failed.
     # It's the live system anyway. It's not that bad...
     git clone https://aur.archlinux.org/ckbcomp.git
     chown -R "nobody:nobody" ckbcomp
     cd ckbcomp
 
     # Patch for now. It's outdated!
     #sed -i "s/1.193/1.194/g" PKGBUILD
     sed -i "s/sha512sums=.*/sha512sums=('SKIP')/" PKGBUILD
     
     sudo -u nobody makepkg -s
 
     pacman -U ckbcomp*.pkg.tar.* --needed --noconfirm
     cd ..
     rm -rf ckbcomp
}

# HAS to be down here because of ckbcomp, if the alternative methods are active.
pacman -Sy emilia-calamares emilia-calamares-config --noconfirm --needed || pacman -Sy emilia-calamares emilia-calamares-config --noconfirm --needed

rm -rf /home/emilialive/.cache

# Now from the menu the theme applies also.
sed -i "s/Exec=.*/Exec=sudo -E calamares/" /usr/share/applications/calamares.desktop
