#!/bin/bash

sed -i "s/#\(autologin-session=\)/\1openbox/" /etc/lightdm/lightdm.conf

systemctl enable lightdm.service
systemctl set-default graphical.target

# Change configs to the Minimal Version.
sed -i "s/netinstall.yaml/netinstallminimal.yaml/g" /etc/calamares/modules/netinstall.conf
sed -i "s/##- internet/- internet/g" /etc/calamares/modules/netinstall.conf

# While we don't figure it out...
#sed -i "s/#  - packagechooser/  - packagechooser/g" /etc/calamares/settings.conf

sed -i "s/disable-cancel:.*/disable-cancel: true/g" /etc/calamares/settings.conf
#sed -i "s/disable-cancel-during-exec:.*/disable-cancel-during-exec: true/g" /etc/calamares/settings.conf

echo 'unpack:
    - source: "/run/emiso/bootmnt/emi/x86_64/rootfs.sfs"
      sourcefs: "squashfs"
      destination: "/"
' > /etc/calamares/modules/unpackfs.conf


# Open calamares on start.
mkdir -p /home/emilialive/.config/openbox/
echo '
sudo calamares &
' > /home/emilialive/.config/openbox/autostart
