#!/bin/bash
#somente root

#which DE is installed...?
desktop=$(</usr/share/emilia/desktop.txt)

total=$(ls -l /run/emiso/bootmnt/emi/x86_64/ | wc -l)
total=$(($total -1))


#if is the all version, add the DE name in calamares unsquashfs module.
if [ "$total" -gt 10 ];then
    sed -i "s/desktopfs.sfs/desktop${desktop}fs.sfs/g" /etc/calamares/modules/unpackfs.conf
fi

# #alsa teste
amixer sset Master unmute
amixer sset Speaker unmute
amixer sset Headphone unmute
