#!/bin/bash

set -e -u

lightdmSetup(){
    #compartilha com o minimal lá
    emilia-garden reset lightdm

    systemctl enable lightdm.service
    systemctl set-default graphical.target
}


lightdmSetup

#try again if the network fails.
pacman -Sy emilia --noconfirm --needed || pacman -Sy emilia --noconfirm --needed

dconf update
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
systemctl enable bluetooth.service

cp -r /etc/skel/. /root/
