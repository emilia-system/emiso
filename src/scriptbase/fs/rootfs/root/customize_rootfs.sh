#!/bin/bash

set -e -u

localeConfig(){
    sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen #o locale fica pt-br sozinho com ele comentado. mas é bom que tenha ele gerado talvez.
    sed -i 's/#\(pt_BR\.UTF-8\)/\1/' /etc/locale.gen
    #locale-gen # agr tá na parte brasil
    
    #ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    #agora é horário de SP, ver parte do brasil
}

rootcfg(){
    cp -aT /etc/skel/ /root/
    chown -R root:root /root
    chmod 700 /root
        
    #quero bash.
    #usermod -s /usr/bin/zsh root
}

brasilSetup(){
    #-------------------------------------------------
    #brasil! prioridade é dele!
    #pra iso, faz um teclado, ajuda

    #TIMEZONE
    # TESTING THE NEW FILE THERE
    #ln -sf /usr/share/zoneinfo/Brazil/East /etc/localtime
    
    #LANG
    locale-gen #só pra garantir

    #KEYBOARD
    #localectl --no-convert set-x11-keymap br pc104 "" "" #é. chroot n rola
    #solução foi jogar no arquivo /etc/X11/xorg.conf.d/00-keyboard.conf


}

pacmanSetup(){
    # Pacman
    rm -rf /usr/share/local-repository/local-repository*
    
    #repo-add /usr/share/local-repository/local-repository.db.tar.gz /usr/share/local-repository/*
    cd /usr/share/local-repository
    repose -zf local-repository
    cd - > /dev/null

    sed -i 's|#EmiliaReplace|# Emilia Only - Local Repository for testings\
[local-repository]\
Server = file:///usr/share/$repo\
SigLevel = Optional TrustAll|' /etc/pacman.conf
    
    sed -i "s/#\(Server\)/\1/g" /etc/pacman.d/mirrorlist
    pacman-key --init
    pacman-key --populate archlinux

    # Blackarch 
    
    # cd /root
    # curl -O https://blackarch.org/strap.sh
    # chmod +x strap.sh
    # ./strap.sh
    # cd /

    # In local repository we added. Remember to re-run pre-build.sh sometimes.
    pacman -Sy yay-bin --noconfirm --needed || pacman -Sy yay-bin --noconfirm --needed
    
    # Clean repo as yay is updated using itself
    rm -rf /usr/share/local-repository/*
    cd /usr/share/local-repository
    repose -zf local-repository
    cd - > /dev/null
    
    # Try again if failed by emilia.db not downloading.
    pkgfile --update || pkgfile --update
    systemctl enable pkgfile-update.timer
}

sudoSetup(){
    #----------------------------------------

    #adiciona o grupo sudo ao sudoers
    echo -e '

#let vim begone from here!
Defaults editor=/usr/bin/nano

%sudo ALL=(ALL) ALL
%admin ALL=(ALL) NOPASSWD: ALL
' >> /etc/sudoers

}

systemSetup(){

    sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
    sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
    
    systemctl set-default multi-user.target
    systemctl enable pacman-init.service choose-mirror.service
    systemctl enable NetworkManager.service

    echo 'NAME="EmiliaOS"
    PRETTY_NAME="Emilia System"
    ID=emilia
    ID_LIKE=archlinux
    BUILD_ID=rolling
    ANSI_COLOR="0;33"
    HOME_URL="<not_yet>"
    DOCUMENTATION_URL="<not_yet>"
    SUPPORT_URL="<not_yet>"
    BUG_REPORT_URL="<not_yet>"' > /usr/lib/os-release
    
    echo 'Emilia System \r (\l)' > /etc/issue
}
groupSetup(){
    groupadd -r sudo
    groupadd -r admin
    groupadd -r autologin
    
    gpasswd -a root audio
}


localeConfig
rootcfg

brasilSetup
pacmanSetup
sudoSetup
systemSetup
groupSetup

# Apply items in skel for root user
cp -r /etc/skel/. /root/
