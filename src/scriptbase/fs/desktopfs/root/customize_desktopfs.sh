#!/bin/bash

set -e -u

lightdmnome=""
# $1 is the desktop name as in $desk in build.sh

case "${1}" in
    gnome)
        lighdmnome="gnome"
        mkdir -p /etc/skel/.config/autostart
        emilia-create-autostart \
            -i \
            -c "/usr/share/emilia/gnome-setup.sh" \
            -n "gnome-setup" \
            -d "Initialize Gnome" \
            -a "/etc/skel/.config/autostart/"
            # exists because of the extension-tool
            
        # Better hack. nobody will compile it, don't care for yay. yay needs a home folder, so it breaks with nobody.
        mkdir -p /compiling
        [ ! -d /compiling/gnome-dash-to-panel/ ] && git clone https://aur.archlinux.org/gnome-shell-extension-dash-to-panel.git /compiling/gnome-dash-to-panel/
        chown -R "nobody" /compiling
        
        # Patch if outdated!
        #sed -i "s/versionold/versionnew/g" PKGBUILD
        #sed -i "s/sha512sums=.*/sha512sums=('SKIP')/" PKGBUILD

        # Requirements. Just to be sure
        pacman -Sy gnome-shell git gnome-common intltool --needed --noconfirm
        cd /compiling/gnome-dash-to-panel/
        # Only the sudo command works runuser and su do not, already tried.
        sudo -u nobody makepkg -s
        
        # The nobody account requires password for sudo, so it can't install the package from the above command with -si.
        pacman -U /compiling/gnome-dash-to-panel/*.pkg.tar.* --needed --noconfirm
        cd /
        rm -rf /compiling
        
        ;;
    plasma)
        lighdmnome="plasma"
        ;;
    xfce)
        lighdmnome="xfce"
        ;;
    cinnamon)
        lighdmnome="cinnamon"
        ;;
    mate)
        lighdmnome="mate"
        # Provisory solution to mate not starting with garden configuration
        mkdir -p /etc/skel/.config/autostart
        emilia-create-autostart \
            -i \
            -c "emilia-garden reset mate" \
            -n "Appearance fix for mate" \
            -d "Mate is not configured by default and we couldn't fix it yet." \
            -a "/etc/skel/.config/autostart/"
        ;;
    lxqt)
        lighdmnome="lxqt"
        ;;
    i3)
        lighdmnome="i3"
        ;;
    budgie)
        lighdmnome="budgie-desktop"
        ;;
    *)
        echo "Invalid desktop in customize_desktopfs.sh"
        exit 1
        ;;
esac

# Remove the meta packages. The end user can be annoyed that if this updates, all the non-usefull packages for him returns. That is a no-no!
pacman -R emilia-meta-$1 emilia-meta-$1-apps --noconfirm

sed -i "s/#\(autologin-session=\)/\1${lighdmnome}/" /etc/lightdm/lightdm.conf

# As root, garden gives a system-wide configuration for NEW USERS! For emilialive and whoever install the system.
emilia-garden reset $1

dconf update # Flatpak used already, but now it generates the emilia database if a dbus desktop entered here.
# Except the mate system. We are studing it.

cp -r /etc/skel/. /root/
