#!/bin/bash

set -e -u

# Configurable Flags (altered via parameters OR profile file)
#  Parameters have priority over the file!
profile_file="profile.sh"

iso_name="EmiliaSystem"
iso_label="EMILIA_$(date +%Y%m)"
iso_publisher="Emilia Team"
iso_application="Emilia Install/Live"
iso_version=$(date +%Y.%m)
install_dir=emi
reset_out_dir=0
work_dir=work
out_dir=out
verbose=0 # Console Output
quiet=0

debian=0
gpg_key=""
# INTERNAL FLAGS -----------
cowspace="2048M"
de_number=0
de_number_bonus=0
de_total=0


# Desktop configuration
all=0
minimal_only=1
script_state="isoNormal"
desktop_list=()
desk=""
old_desk=""

# Bonus colors
yellow="\e[93;1m"
blue="\e[34;1m"
magenta="\e[35;1m"
green="\e[32;1m"
resetcolor="\e[0m"
white="\e[97m"
red="\e[31m"
lightcyan="\e[97m"

emicolor=${yellow}

script_path=$(readlink -f "${0%/*}")

# Language Setup
. gettext.sh

export TEXTDOMAIN=emilia-iso
export TEXTDOMAINDIR=/usr/share/locale
umask 0022

show_step(){
    echo -e "${white}----------------------------------------------${resetcolor}"
    echo 
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${white}" "$(eval_gettext "Checkpoint!")" "${resetcolor}"
    echo
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${white}" "$(eval_gettext "Step")" "${resetcolor}: $1."
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${white}" "$(eval_gettext "Description")" "${resetcolor}: $2."
    echo
    
    if [ ${verbose} -eq 1 ]; then
        echo -e "[${emicolor}EmiliaBuilder${resetcolor}] ${white}DEBUG INFO:${resetcolor}"
        echo " - SCRIPT STEP = ${script_state}"
        echo " - OLD DESK = ${old_desk}"
        echo " - DESK = ${desk}"
        echo " - ONLYMINIMAL = ${minimal_only}"
        echo 
    fi
}

show_error(){
    echo -e "${white}----------------------------------------------${resetcolor}"
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${red}" "$(eval_gettext "Critical Failure!")" "${resetcolor}"
    echo 
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${white}" "$(eval_gettext "Error")" "${resetcolor}: $1."
    echo -e "[${emicolor}EmiliaBuilder${resetcolor}]${white}" "$(eval_gettext "Sugestion")" "${resetcolor}: $2."
    echo 
}

_usage () {
    echo "$(eval_gettext "Usage: ./build.sh [options]")"
    echo
    echo "   $(eval_gettext "Common Options:")"
    echo "    -n   $(eval_gettext "<iso_name>")"
    echo "                         $(eval_gettext "Give a name for the ISO (prefix).")"
    echo "                          $(eval_gettext "Default:")   ${iso_name}"
    echo "    -p   $(eval_gettext "<publisher>")"
    echo "                         $(eval_gettext "Creator of this ISO.")"
    echo "                          $(eval_gettext "Default:")   ${iso_publisher}"
    echo "    -m   $(eval_gettext "<cow_memory>")" 
    echo "                         $(eval_gettext "Gives a value for cowspace in memory.")"
    echo "                          $(eval_gettext "Default:")   ${cowspace}"
    echo "    -P   $(eval_gettext "<profile_file>")"
    echo "                         $(eval_gettext "Enable Custom Profile File")"
    echo "                          $(eval_gettext "Default:")   ${profile_file}"
    echo
    echo "    -d                   $(eval_gettext "Enable Debian Version")   . Comming soon...?"
    echo "    -v                   $(eval_gettext "Enable Verbose, for more Debug info.")"
    echo "    -c                   $(eval_gettext "Cleaning/Deletion of the output folder.")"
    echo "    -h                   $(eval_gettext "This help message.")"
    echo
    echo "      $(eval_gettext "About Desktop Enviroments...")"
    echo
    echo "    -N                   $(eval_gettext "Enable Minimal/NetInstall iso")"
    echo "    -G                   $(eval_gettext "Enable GNOME iso")"
    echo "    -K                   $(eval_gettext "Enable KDE-Plasma iso")"
    echo "    -X                   $(eval_gettext "Enable XFCE iso")"
    echo "    -C                   $(eval_gettext "Enable Cinnamon iso")"
    echo "    -M                   $(eval_gettext "Enable MATE iso")"
    echo "    -L                   $(eval_gettext "Enable LXQT iso")"
    echo "    -B                   $(eval_gettext "Enable Budgie iso")"
    echo
    echo "    $(eval_gettext "Now, to the insane part!")"
    echo
    echo -e "    ${blue}-A${resetcolor}                   $(eval_gettext "Enable ALL ISOS - Use only this option, not the individual isos!")"
    echo -e "    ${magenta}-a${resetcolor}                   $(eval_gettext "Enable ALL ISOS... in a unique ISO.")"
    echo
    #echo "    -I                   $(eval_gettext "BONUS: Enable i3 iso")" "<- currently disabled..."
    #echo "    -O                   $(eval_gettext "Enable Openbox iso")" "<- currently disabled..."
    exit "${1}"
}

# Helper function to run make_*() only one time per architecture.
run_once() {
    if [ ! -e "${work_dir}/build.${1}" ]; then
        "$1"
        touch "${work_dir}/build.${1}"
    fi
}

# LOAD PROFILE FIRST --------------
if [ -f "$profile_file" ];then
    # Use as absolute path
    source "$profile_file" 
elif [ -f "$(pwd)/$profile_file" ]; then
    # Use as relative path...?
    source "$(pwd)/$profile_file"
elif [ -f "${script_path}/$profile_file" ]; then
    # Use as a file inside the script folder!
    source "${script_path}/$profile_file"
else
    # Come on... using the default inside this file then.
    # TODO Color me!
    echo  "->" "$(eval_gettext "Profile file not found! Using Default Profile inside the build.sh file!")"
fi

while getopts 'n:p:g:m:dcvqAaNGKXCMLQBIh?' arg; do
    case "${arg}" in
        n) iso_name="${OPTARG}" ;;
        p) iso_publisher="${OPTARG}" ;;
        g) gpg_key="${OPTARG}" ;;
        m) cowspace="${OPTARG}" ;;
        c) reset_out_dir=1 ;;
        d) debian=1 ;;
        v) verbose=1 ;;
        q) 
            quiet=$((quiet + 1)) 
        ;;

        A) 
            emicolor="${blue}"
            echo -e "${emicolor}-A${resetcolor}." "$(eval_gettext "Wow. All ISOs? Ok!")"
            
            desktop_list+=("isoMinimal")
            desktop_list+=("gnome")
            desktop_list+=("plasma")
            desktop_list+=("xfce")
            desktop_list+=("cinnamon")
            desktop_list+=("mate")
            desktop_list+=("lxqt")
            desktop_list+=("budgie")
            #desktop_list+=("i3")
            
            de_number=$((de_number + 8))
            minimal_only=0
            ;;
        a) 
            echo -e "${magenta}-a${resetcolor}." "$(eval_gettext "This is insane... One ISO with all! Can use some disk space on generation...")"
            all="1"
            minimal_only=0
            ;;
        N) 
            echo -e "${lightcyan}-N${resetcolor}." "$(eval_gettext "Minimal Iso on the list!")"
            desktop_list+=("isoMinimal")
            de_number+=$((de_number + 1))
            ;;
        G) 
            desktop_list+=("gnome")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        K) 
            desktop_list+=("plasma")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        X) 
            desktop_list+=("xfce")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        C) 
            desktop_list+=("cinnamon")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        M) 
            desktop_list+=("mate")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        L) 
            desktop_list+=("lxqt")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
        B) 
            desktop_list+=("budgie")
            de_number=$((de_number + 1))
            minimal_only=0
            ;;
#         O) 
#             desktop_list+=("openbox")
#             de_number=$((de_number + 1))
#             minimal_only=0
#             ;;
#         I) 
#             desktop_list+=("i3")
#             de_number_bonus+=$((de_number_bonus + 1))
#             minimal_only=0
#             ;;
        h|?) _usage 0 ;;
        *)
           show_error "$(eval_gettext "Invalid argument '\${arg}'." "Please, use one from the valid option list.")"
           _usage 1
           ;;
    esac
done

####################################
    if [ ${debian} -eq 2 ]; then # disabled until something happens.
        # Emilia 8 - Doll Version
        # Debian Version - not happening so soon
        source "${script_path}/debian-functions.sh"
    else 
        # Arch Default version
        source "${script_path}/arch-functions.sh" 
    fi
####################################

if [ ${EUID} -ne 0 ]; then
    show_error "$(eval_gettext "Run this script as root. Use sudo!")" "$(eval_gettext "This script requires sudo or root account running it, so it can use some root-only required feature. Try to put 'sudo' before the command.")"
    _usage 1
fi
if [ ${de_number} -eq 0 -a ${de_number_bonus} -eq 0 ]; then
    show_error "$(eval_gettext "Bad usage. Use at least one of the desktop enviroments.")" "$(eval_gettext "Be sure to use a option from the help list!")"
    echo
    _usage 1
fi
if [ ${de_number} -ge 9 ]; then
    show_error "$(eval_gettext "Bad usage. -A is used alone.")" "$(eval_gettext "Please, select only -A and none from the other desktops. -a is the exception.")"
    echo
    _usage 2
fi

# Reset the output folder if asked to
if [ ${reset_out_dir} -eq 1 ]; then
    rm -rf ${out_dir}
    mkdir -p ${out_dir}/package_list
fi

run_once make_local_repository
run_once make_pacman_conf
run_once make_base_install
run_once make_base_config

run_once make_setup_mkinitcpio
run_once make_gen_initram

run_once make_boot
run_once make_syslinux
run_once make_isolinux
run_once make_efi
run_once make_efiboot

if [ ${minimal_only} -eq 1 ]; then
    [ $quiet -eq 0 ] && echo -e "${red}WARNING${resetcolor}:" "$(eval_gettext "Only Minimal detected. Skipping Common generation")"
else
    run_once make_common_install
    run_once make_common_config
fi

for _de in "${desktop_list[@]}"; do
    if ! [ -e "${work_dir}/desk.${_de}" ]; then
        old_desk="${desk}"
        desk="${_de}"
        
        if [ "${desk}" != "isoMinimal" ]; then
            run_once make_desktop_install
            run_once make_desktop_config
        fi
        run_once make_live_install
        run_once make_live_config
        
#         if [ "${old_desk}" = "isoMinimal" -o "${desk}" = "isoMinimal" ]; then
#             make_gen_initram
#         fi
 
        run_once make_cleanup
        run_once make_prepare_base
        if [ "${desk}" != "isoMinimal" ]; then 
            run_once make_prepare_common #Really RUNONCE
            run_once make_prepare_de
        fi
        run_once make_prepare_live
        
        run_once make_iso
        
        make_reset_de
        #it's done! moving on!
        touch ${work_dir}/desk."${_de}"
    else
        [ $quiet -eq 0 ] && echo "$(eval_gettext "MESSAGE: \${_de} was done before. Skip!")"
    fi
    de_number=$((de_number - 1))
    de_total=$((de_number + de_number_bonus))
    [ $quiet -eq 0 ] && echo "$(eval_gettext "${de_total} Desktop Enviroments Isos remaining...")"
done

all_desktops=()
all_desktops+=("isoMinimal")
all_desktops+=("gnome")
all_desktops+=("plasma")
all_desktops+=("xfce")
all_desktops+=("cinnamon")
all_desktops+=("mate")
all_desktops+=("lxqt")
all_desktops+=("budgie")

if [ ${all} -eq 1 ]; then
    [ $quiet -eq 0 ] && echo "$(eval_gettext "It's time... ISO All-in-one.")"
    
    emicolor="${magenta}"
    script_state="AllInOne"
    desk="All" # Start this part as All in desktop if it's needed at first
    
    # Runned first just to reset initram generation...
    rm -f "${work_dir}"/build.make_gen_initram
    
    for _de in "${all_desktops[@]}"; do
        desk="${_de}"
        if ! [ -e ${work_dir}/tempiso/liveonly"${desk}"fs.sfs ]; then 

            if [ "${desk}" != "isoMinimal" ]; then
                run_once make_desktop_install
                run_once make_desktop_config
            fi

            run_once make_live_install
            run_once make_live_config

            run_once make_cleanup
            run_once make_prepare_base # Really RUNONCE
            run_once make_prepare_common # Really RUNONCE
            
            if [ "${desk}" != "isoMinimal" ]; then
                run_once make_prepare_de
            fi
            run_once make_prepare_live 

            run_once make_gen_initram

            make_reset_de

        else
            [ $quiet -eq 0 ] && echo "$(eval_gettext "One of the isos can be used here. Doing initram for \${_de}.")"
            make_gen_initram
        fi
    done
    
    # Add syslinux
    rm -f "${work_dir}"/iso/"${install_dir}"/boot/syslinux/emiso_sys.cfg 
    mv "${work_dir}"/emiso_sys_all.cfg "${work_dir}"/iso/"${install_dir}"/boot/syslinux/emiso_sys.cfg
    
    # Remove old initcpio from the base
    rm -f "${work_dir}"/iso/"${install_dir}"/boot/x86_64/emiso.img
    
    # Move isos and hashes from temp to the oficial
    mv "${work_dir}"/tempiso/*.sfs "${work_dir}"/iso/"${install_dir}"/x86_64/
    mv "${work_dir}"/tempiso/*.sha512 "${work_dir}"/iso/"${install_dir}"/x86_64/
    
    desk="AllInOne"
    run_once make_iso
fi

[ $quiet -eq 0 ] && echo "$(eval_gettext "Removing work folder... (\${work_dir})")"
rm -rf "${work_dir}"
echo "$(eval_gettext "Script finished! Go to the folder \${out_dir} to get your ISOs!")"
