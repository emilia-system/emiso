# Put your custom info here. The system will use it automatically.

# Iso_name - Gives the final name to the iso before the -<desktop>
# Iso_label - The internal label of the iso.
# iso_publisher - The publisher of this iso.
# iso_application - The description of what this iso should do
# iso_version - The versioning of the iso. As Arch Linux is rolling release, they and Emilia decided to use the current date.
# install_dir - WARNING - DO NOT change this yet! part of the code inside the hooks of the initram expect the word "emi". there are some parts that depends on in. Will change in the future!
# reset_out_dir - Delete the output directory before starting, so it can reset the output. It's not mandatory, it's just for convenience.
# work_dir - the name of the working directory where the work is done for generating the iso. It will be created on the current working directory (as returned by pwd). We recommend running build.sh on the directory it is.
# out_dir - same of work_dir, but referers to the output files.
# verbose - shows a bit more info. Not much but it's something
# quiet - this can have many tiers. 1 shuts the normal speaches. 2 shuts everything off almost!

# This file has the same configurations by default compared to build.sh.
iso_name="EmiliaSystem"
iso_label="EMILIA_$(date +%Y%m)"
iso_publisher="Emilia Team"
iso_application="Emilia Install/Live"
iso_version=$(date +%Y.%m)
#install_dir=emi
reset_out_dir=0
work_dir=work
out_dir=out
verbose=0
quiet=0
