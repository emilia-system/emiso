#!/bin/bash

set -e -u

cd "$(readlink -f "${0%/*}")"

mkdir -p pkg-building repository

#gitlab -- Emilia
#git clone https://gitlab.com/emilia-system/repository.git
#rm -rf repository/.git
#rm -f repository/LICENSE repository/README.md


#AUR
cd pkg-building
for _pkg in yay-bin pamac-aur ckbcomp; do
    if ! [ -f "../repository/${_pkg}*" ]; then
        git clone https://aur.archlinux.org/${_pkg}.git
        cd ${_pkg}
        makepkg -s
        
        mv *.pkg.tar.xz ../../repository/
        cd ..
        rm -rf ${_pkg}
    fi
done
cd ..



rm -rf pkg-building
